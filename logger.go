// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

package urlshorten

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/ssh/terminal"
)

var log = logrus.StandardLogger()

// self defined formatting
type MyTextFormatter struct {
}

func (f *MyTextFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	var b *bytes.Buffer
	keys := make([]string, 0, len(entry.Data))
	for k := range entry.Data {
		keys = append(keys, k)
	}
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	levelText := strings.ToUpper(entry.Level.String())
	timestamp := entry.Time.Format(DateTimeLayout)
	b.WriteString(levelText[:4])
	b.WriteByte(' ')
	b.WriteString(timestamp)
	for i := len(DateTimeLayout) - len(timestamp); i > 0; i-- {
		b.WriteByte('0') // padding
	}
	b.WriteByte(' ')
	b.WriteString(entry.Message)
	for _, key := range keys {
		b.WriteString(key)
		b.WriteByte('=')
		b.WriteString(fmt.Sprintf("%v", entry.Data[key]))
		b.WriteByte(' ')
	}
	b.WriteByte('\n')
	return b.Bytes(), nil
}

func isWriterTerminal(w io.Writer) bool {
	switch v := w.(type) {
	case *os.File:
		return terminal.IsTerminal(int(v.Fd()))
	default:
		return false
	}
}

func SetupLog(loglevel, filepath string, diyformat bool) error {
	var logger = logrus.StandardLogger()
	level, err := logrus.ParseLevel(loglevel)
	if err != nil {
		return err
	}
	logrus.SetLevel(level)

	var isTerminal = isWriterTerminal(logger.Out)
	var isColored = false
	if isTerminal {
		diyformat = false
		isColored = true
	}

	if runtime.GOOS == "windows" {
		isColored = false
		diyformat = true
	}

	// log format
	if diyformat {
		logger.Formatter = &MyTextFormatter{}
	} else {
		logger.Formatter = &logrus.TextFormatter{
			FullTimestamp:   true,
			TimestampFormat: DateTimeLayout,
			ForceColors:     isColored,
		}
	}

	// write log to file under windows platform
	if filepath != "" && runtime.GOOS == "windows" {
		logger.AddHook(NewFileLogHook(filepath))
	}
	return nil
}
