package urlshorten

import (
	"encoding/json"
	"io/ioutil"

	"github.com/hjson/hjson-go"
)

type RedisConfig struct {
	ServerAddr    string `json:"server-addr"`
	KeyPrefix     string `json:"key-prefix"`
	MaxActiveConn int    `json:"max-active-conn"`
	MaxIdleConn   int    `json:"max-idle-conn"`
	IdleTimeout   int    `json:"idle-timeout"`
}

type MySQLConfig struct {
	Host         string `json:"host"`
	Port         uint16 `json:"port"`
	Database     string `json:"database"`
	User         string `json:"user"`
	Password     string `json:"passwd"`
	Table        string `json:"table"`
	ReadTimeout  int    `json:"read-timeout"`
	WriteTimeout int    `json:"write-timeout"`
}

type Config struct {
	Addr          string       `json:"addr"`
	ShortenUrl    string       `json:"shorten-url"`
	ShortenTTL    int          `json:"shorten-ttl"`
	MaxUrlLength  int          `json:"max-url-length"`
	LogLevel      string       `json:"log-level"`
	LogFilePath   string       `json:"log-file-path"`
	WorkerID      int          `json:"worker-id"`
	PprofPort     int          `json:"pprof-port"`
	EnableCache   bool         `json:"enable-cache"`
	CacheExpire   int          `json:"cache-expire"`
	CacheCapacity int          `json:"cache-capacity"`
	Storage       string       `json:"storage"`
	Redis         *RedisConfig `json:"redis"`
	MySQL         *MySQLConfig `json:"mysql"`
}

func (c *Config) LoadFromFile(filename string) error {
	rawbytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	var dat map[string]interface{}
	if err := hjson.Unmarshal(rawbytes, &dat); err != nil {
		return err
	}
	rawdata, err := json.Marshal(dat)
	if err != nil {
		return err
	}
	return json.Unmarshal(rawdata, c)
}

func NewConfig() *Config {
	return &Config{
		Addr:          ":8081",
		LogLevel:      "debug",
		LogFilePath:   "shorten",
		MaxUrlLength:  250,
		EnableCache:   true,
		CacheExpire:   30 * 60, // 30min
		CacheCapacity: 6000,
	}
}
