// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

package urlshorten

import (
	"crypto/sha1"
	"database/sql"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/go-sql-driver/mysql"
)

var eNonRowsAffected = errors.New("non rows affected")

type MySQLStorage struct {
	config    *Config
	cache     map[string]*UrlRecord
	cacheHash map[string]*UrlRecord
	mtx       sync.Mutex
	db        *sql.DB
}

func NewMySQLStorage() Storage {
	return &MySQLStorage{}
}

func makeDSN(config *Config) string {
	var mycfg = config.MySQL
	var cfg = mysql.NewConfig()
	cfg.Net = "tcp"
	cfg.Addr = fmt.Sprintf("%s:%d", mycfg.Host, mycfg.Port)
	cfg.DBName = mycfg.Database
	cfg.User = mycfg.User
	cfg.Passwd = mycfg.Password
	cfg.ParseTime = true
	cfg.InterpolateParams = true
	cfg.MultiStatements = true
	cfg.Loc = time.Local
	cfg.Timeout = time.Second * 15
	if mycfg.ReadTimeout > 0 {
		cfg.ReadTimeout = time.Second * time.Duration(mycfg.ReadTimeout)
	}
	if mycfg.WriteTimeout > 0 {
		cfg.WriteTimeout = time.Second * time.Duration(mycfg.WriteTimeout)
	}
	return cfg.FormatDSN()
}

func (s *MySQLStorage) Start(config *Config) error {
	s.config = config
	s.cache = make(map[string]*UrlRecord, config.CacheCapacity)
	s.cacheHash = make(map[string]*UrlRecord, config.CacheCapacity)
	var dsn = makeDSN(config)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return err
	}
	if err := db.Ping(); err != nil {
		return err
	}
	s.db = db
	return nil
}

func (s *MySQLStorage) Close() {
	if s.db != nil {
		s.db.Close()
		s.db = nil
	}
}

func (s *MySQLStorage) findCache(shortUrl string) *UrlRecord {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	var rec = s.cache[shortUrl]
	if rec != nil {
		createTime, err := time.Parse(DateTimeLayout, rec.CreateTime)
		if err != nil {
			log.Errorf("findCache: %v, %s", err, rec.CreateTime)
			return nil
		}
		var now = time.Now()
		if now.Sub(createTime).Seconds() > float64(rec.Ttl) {
			delete(s.cache, shortUrl)
			return nil
		}
		return rec
	}
	return nil
}

func (s *MySQLStorage) findHashCache(hash string) *UrlRecord {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	var rec = s.cacheHash[hash]
	if rec != nil {
		createTime, err := time.Parse(DateTimeLayout, rec.CreateTime)
		if err != nil {
			log.Errorf("findHashCache: %v, %s", err, rec.CreateTime)
			return nil
		}
		var now = time.Now()
		if now.Sub(createTime).Seconds() > float64(rec.Ttl) {
			delete(s.cacheHash, hash)
			return nil
		}
		return rec
	}
	return nil
}

func (s *MySQLStorage) QueryUrlRecord(shortUrl string) (*UrlRecord, error) {
	// test if in cache
	if s.config.EnableCache {
		if rec := s.findCache(shortUrl); rec != nil {
			return rec, nil
		}
	}
	if err := s.db.Ping(); err != nil {
		return nil, err
	}
	var sqlstmt = "SELECT `id`, `long_url`, `hash`, `ttl`, `create_ip`, `create_time` FROM `%s` WHERE `short_url`=?"
	stmt, err := s.db.Prepare(fmt.Sprintf(sqlstmt, s.config.MySQL.Table))
	if err != nil {
		return nil, err
	}
	var rec UrlRecord
	if err = stmt.QueryRow(shortUrl).Scan(&rec.ID, &rec.LongUrl, &rec.Hash, &rec.Ttl, &rec.CreateIP, &rec.CreateTime); err != nil {
		if err == sql.ErrNoRows {
			err = nil
		}
		return nil, err
	}
	return &rec, nil
}

func (s *MySQLStorage) QueryUrlRecordBy(longUrl string) (*UrlRecord, error) {
	var hash = fmt.Sprintf("%x", sha1.Sum([]byte(longUrl)))
	// test if in cache
	if s.config.EnableCache {
		if rec := s.findHashCache(hash); rec != nil {
			return rec, nil
		}
	}
	if err := s.db.Ping(); err != nil {
		return nil, err
	}
	var sqlstmt = "SELECT `id`, `short_url`, `ttl`, `create_ip`, `create_time` FROM `%s` WHERE `hash`=?"
	stmt, err := s.db.Prepare(fmt.Sprintf(sqlstmt, s.config.MySQL.Table))
	if err != nil {
		return nil, err
	}
	var rec UrlRecord
	if err = stmt.QueryRow(hash).Scan(&rec.ID, &rec.ShortUrl, &rec.Ttl, &rec.CreateIP, &rec.CreateTime); err != nil {
		if err == sql.ErrNoRows {
			err = nil
		}
		return nil, err
	}
	rec.Hash = hash
	rec.LongUrl = longUrl
	return &rec, nil
}

func (s *MySQLStorage) SaveShortenUrl(rec *UrlRecord) error {
	var hash = fmt.Sprintf("%x", sha1.Sum([]byte(rec.LongUrl)))
	rec.Hash = hash

	if err := s.db.Ping(); err != nil {
		return err
	}
	var sqlstmt = "INSERT INTO `%s`(`id`, `short_url`, `long_url`, `hash`, `ttl`, `api_key`,`create_ip`, `create_time`)" +
		"VALUES(?, ?, ?, ?, ?, ?, ?, ?)"
	stmt, err := s.db.Prepare(fmt.Sprintf(sqlstmt, s.config.MySQL.Table))
	if err != nil {
		return err
	}
	result, err := stmt.Exec(rec.ID, rec.ShortUrl, rec.LongUrl, rec.Hash, rec.Ttl, "", rec.CreateIP, rec.CreateTime)
	if err != nil {
		return err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected == 0 {
		return eNonRowsAffected
	}
	return nil
}
