// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

package urlshorten

import (
	"bytes"
	"fmt"
	"os"
	"reflect"
	"runtime"
	"time"
	"unicode"
	"unsafe"
)

const DateTimeLayout = "2006-01-02 15:04:05.999"

func FastBytesToString(b []byte) string {
	if b != nil {
		bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
		sh := reflect.StringHeader{Data: bh.Data, Len: bh.Len}
		return *(*string)(unsafe.Pointer(&sh))
	}
	return ""
}

// DO NOT MODIFY returned bytes
func FastStringToBytes(s string) []byte {
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh := reflect.SliceHeader{Data: sh.Data, Len: sh.Len, Cap: 0}
	return *(*[]byte)(unsafe.Pointer(&bh))
}

func isValidShortUrl(url string) bool {
	if url == "" {
		return false
	}
	for _, ch := range url {
		if !unicode.IsDigit(ch) && !unicode.IsLetter(ch) {
			return false
		}
	}
	return true
}

func Catch() {
	if v := recover(); v != nil {
		Backtrace(v)
	}
}

func Backtrace(message interface{}) {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "Traceback[%s] (most recent call last):\n", time.Now().Format(DateTimeLayout))
	for i := 0; ; i++ {
		pc, file, line, ok := runtime.Caller(i + 1)
		if !ok {
			break
		}
		fmt.Fprintf(&buf, "% 3d. %s() %s:%d\n", i, runtime.FuncForPC(pc).Name(), file, line)
	}
	fmt.Fprintf(&buf, "%v\n", message)
	buf.WriteTo(os.Stderr)
}
