package urlshorten

import (
	"encoding/json"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

type HttpServer struct {
	fasthttp.Server
	router *fasthttprouter.Router
	store  Storage
	config *Config
	sf     *SnowFlake
}

func NewHttpServer(store Storage, config *Config) *HttpServer {
	var sf = NewSnowFlake(4, 4, uint16(config.WorkerID), false)
	var router = fasthttprouter.New()
	router.HandleMethodNotAllowed = true
	var s = &HttpServer{
		Server: fasthttp.Server{
			Handler: router.Handler,
		},
		store:  store,
		config: config,
		router: router,
		sf:     sf,
	}
	router.GET("/go/:url", s.handleGetIndex)
	router.POST("/go/:url", s.handlePostIndex)
	router.GET("/api/shorten", s.handleUrlShorten)
	router.POST("/api/shorten", s.handleUrlShorten)
	return s
}

type ShortenResponse struct {
	Code  int    `json:"code"`
	Url   string `json:"url"`
	Error string `json:"error,omitempty"`
}

func (r *ShortenResponse) Write(ctx *fasthttp.RequestCtx) error {
	ctx.Response.Header.Add("Content-Type", "application/json")
	if data, err := json.Marshal(r); err != nil {
		return err
	} else {
		ctx.SetBody(data)
		return err
	}
}

func (s *HttpServer) Start() {
	go s.serve()
}

func (s *HttpServer) serve() {
	if err := s.ListenAndServe(s.config.Addr); err != nil {
		log.Errorf("ListenAndServe: %v", err)
	}
}

//访问短域名
func (s *HttpServer) handleGetIndex(ctx *fasthttp.RequestCtx) {
	var url = ctx.UserValue("url").(string)
	var resp = &ShortenResponse{}
	if !isValidShortUrl(url) {
		resp.Code = -1
		resp.Error = "invalid url param"
		resp.Write(ctx)
		return
	}
	record, err := s.store.QueryUrlRecord(url)
	if err != nil {
		log.Errorf("QueryUrlRecord: %v", err)
		resp.Code = -2
		resp.Error = "short url query error"
		resp.Write(ctx)
		return
	}
	if record == nil || record.ID == 0 {
		resp.Code = -3
		resp.Error = "url record not found"
		resp.Write(ctx)
		return
	}
	ctx.Redirect(record.LongUrl, 301)
}

func (s *HttpServer) handlePostIndex(ctx *fasthttp.RequestCtx) {
	var url = ctx.UserValue("url").(string)
	var resp = &ShortenResponse{}
	defer resp.Write(ctx)

	if !isValidShortUrl(url) {
		resp.Code = -1
		resp.Error = "invalid url param"
		resp.Write(ctx)
		return
	}
	record, err := s.store.QueryUrlRecord(url)
	if err != nil {
		log.Errorf("QueryUrlRecord: %v", err)
		resp.Code = -2
		resp.Error = "short url query error"
		return
	}
	if record == nil || record.ID == 0 {
		resp.Code = -3
		resp.Error = "url target not found"
		return
	}
	resp.Url = record.LongUrl
}

//生成短域名
func (s *HttpServer) handleUrlShorten(ctx *fasthttp.RequestCtx) {
	var resp = &ShortenResponse{}
	defer resp.Write(ctx)

	var url = FastBytesToString(ctx.FormValue("url"))
	if url == "" || len(url) > s.config.MaxUrlLength {
		resp.Code = -1
		resp.Error = "invalid url param"
		return
	}

	if s.config.ShortenUrl != "" && strings.Index(url, s.config.ShortenUrl) == 0 {
		resp.Url = url
		return
	}

	record, err := s.store.QueryUrlRecordBy(url)
	if err != nil {
		log.Errorf("QueryUrlRecord: %v", err)
		resp.Code = -2
		resp.Error = "short url query error"
	}
	if record != nil && record.ID != 0 {
		resp.Url = s.config.ShortenUrl + record.ShortUrl
		return
	}

	ttl, err := strconv.Atoi(FastBytesToString(ctx.FormValue("ttl")))
	if ttl <= 0 || (s.config.ShortenTTL > 0 && ttl > s.config.ShortenTTL) {
		ttl = s.config.ShortenTTL
	}

	//创建新记录
	var uuid = s.sf.Next()
	var short = EncodeToBase62String(int64(uuid))
	var now = time.Now()
	record = &UrlRecord{
		ID:         uuid,
		LongUrl:    url,
		ShortUrl:   short,
		Ttl:        ttl,
		CreateIP:   RemoteIPAddress(ctx),
		CreateTime: now.Format(DateTimeLayout),
	}
	if err := s.store.SaveShortenUrl(record); err != nil {
		resp.Error = err.Error()
		return
	}
	resp.Url = s.config.ShortenUrl + record.ShortUrl
}

// Remote IP address of request
func RemoteIPAddress(ctx *fasthttp.RequestCtx) string {
	var ip string
	// test if under reverse proxy
	if value := FastBytesToString(ctx.Request.Header.Peek("X-Forwarded-For")); value != "" {
		var idx = strings.Index(value, ",")
		if idx <= 0 {
			ip = value
		} else {
			ip = value[:idx]
		}
	}
	if ip == "" {
		if value := FastBytesToString(ctx.Request.Header.Peek("X-Real-IP")); value != "" {
			ip = value
		}
	}
	if ip == "" {
		ip, _, _ = net.SplitHostPort(ctx.RemoteAddr().String())
	}
	return ip
}
