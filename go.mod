module urlshorten

go 1.12

require (
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/hjson/hjson-go v3.0.0+incompatible
	github.com/ichenq/urlshorten v0.0.0-20190818133916-3f44e3a9ce16
	github.com/sirupsen/logrus v1.4.2
	github.com/valyala/fasthttp v1.4.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	google.golang.org/appengine v1.6.1 // indirect
)
