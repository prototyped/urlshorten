// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

package urlshorten

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"github.com/gomodule/redigo/redis"
)

type RedisStore struct {
	config    *Config
	cache     map[string]*UrlRecord
	cacheHash map[string]*UrlRecord
	mtx       sync.Mutex
	pool      *redis.Pool
}

func NewRedisStore() Storage {
	return &RedisStore{}
}

func (s *RedisStore) Start(config *Config) error {
	s.cache = make(map[string]*UrlRecord, config.CacheCapacity)
	s.cacheHash = make(map[string]*UrlRecord, config.CacheCapacity)
	s.config = config
	s.pool = &redis.Pool{
		MaxActive:   config.Redis.MaxActiveConn,
		MaxIdle:     config.Redis.MaxIdleConn,
		IdleTimeout: time.Duration(config.Redis.IdleTimeout) * time.Second,
		Wait:        true,
		Dial: func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp", config.Redis.ServerAddr)
			return conn, err
		},
	}
	// test connection
	var conn = s.pool.Get()
	conn.Close()
	return nil
}

func (s *RedisStore) Close() {
	s.pool.Close()
}

func (s *RedisStore) findCache(shortUrl string) *UrlRecord {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	var rec = s.cache[shortUrl]
	if rec != nil {
		createTime, err := time.Parse(DateTimeLayout, rec.CreateTime)
		if err != nil {
			log.Errorf("findCache: %v, %s", err, rec.CreateTime)
			return nil
		}
		var now = time.Now()
		if now.Sub(createTime).Seconds() > float64(rec.Ttl) {
			delete(s.cache, shortUrl)
			return nil
		}
		return rec
	}
	return nil
}

func (s *RedisStore) findHashCache(hash string) *UrlRecord {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	var rec = s.cacheHash[hash]
	if rec != nil {
		createTime, err := time.Parse(DateTimeLayout, rec.CreateTime)
		if err != nil {
			log.Errorf("findHashCache: %v, %s", err, rec.CreateTime)
			return nil
		}
		var now = time.Now()
		if now.Sub(createTime).Seconds() > float64(rec.Ttl) {
			delete(s.cacheHash, hash)
			return nil
		}
		return rec
	}
	return nil
}

// 根据短地址查找
func (s *RedisStore) QueryUrlRecord(shortUrl string) (*UrlRecord, error) {
	// test if in cache
	if s.config.EnableCache {
		if rec := s.findCache(shortUrl); rec != nil {
			return rec, nil
		}
	}

	var conn = s.pool.Get()
	if conn.Err() != nil {
		return nil, conn.Err()
	}
	defer conn.Close()
	var key = fmt.Sprintf("%s.short.%s", s.config.Redis.KeyPrefix, shortUrl)
	data, err := redis.Bytes(conn.Do("GET", key))
	if err != nil {
		if err == redis.ErrNil {
			err = nil
		}
		return nil, err
	}
	var record UrlRecord
	if err = json.Unmarshal(data, &record); err != nil {
		return nil, err
	}
	return &record, nil
}

// 根据长地址查找短地址
func (s *RedisStore) QueryShortUrlByHash(longUrl string) (string, error) {
	var hash = fmt.Sprintf("%x", sha1.Sum([]byte(longUrl)))

	// test if in cache
	if s.config.EnableCache {
		if rec := s.findHashCache(hash); rec != nil {
			return rec.LongUrl, nil
		}
	}

	var conn = s.pool.Get()
	if conn.Err() != nil {
		return "", conn.Err()
	}
	defer conn.Close()
	var key = fmt.Sprintf("%s.hash.%s", s.config.Redis.KeyPrefix, hash)
	url, err := redis.String(conn.Do("GET", key))
	if err == redis.ErrNil {
		err = nil
	}
	return url, err
}

// 根据长地址查找
func (s *RedisStore) QueryUrlRecordBy(longUrl string) (*UrlRecord, error) {
	shortUrl, err := s.QueryShortUrlByHash(longUrl)
	if err != nil {
		return nil, err
	}
	if shortUrl == "" {
		return nil, nil
	}

	var conn = s.pool.Get()
	if conn.Err() != nil {
		return nil, conn.Err()
	}
	defer conn.Close()
	var key = fmt.Sprintf("%s.short.%s", s.config.Redis.KeyPrefix, shortUrl)
	data, err := redis.Bytes(conn.Do("GET", key))
	if err != nil {
		if err == redis.ErrNil {
			err = nil
		}
		return nil, err
	}
	var record UrlRecord
	if err = json.Unmarshal(data, &record); err != nil {
		return nil, err
	}
	return &record, nil
}

//保存短地址记录
func (s *RedisStore) SaveShortenUrl(record *UrlRecord) error {
	var hash = fmt.Sprintf("%x", sha1.Sum([]byte(record.LongUrl)))
	record.Hash = hash

	data, err := json.Marshal(record)
	if err != nil {
		return err
	}
	var key = fmt.Sprintf("%s.short.%s", s.config.Redis.KeyPrefix, record.ShortUrl)
	var conn = s.pool.Get()
	if conn.Err() != nil {
		return conn.Err()
	}
	defer conn.Close()
	if record.Ttl >= 0 {
		_, err = conn.Do("SET", key, data)
	} else {
		_, err = conn.Do("SETEX", key, record.Ttl, data)
	}
	if err != nil {
		return err
	}

	var key2 = fmt.Sprintf("%s.hash.%s", s.config.Redis.KeyPrefix, hash)
	if record.Ttl >= 0 {
		_, err = conn.Do("SET", key2, record.ShortUrl)
	} else {
		_, err = conn.Do("SETEX", key2, record.Ttl, record.ShortUrl)
	}

	// update cache
	if err == nil && s.config.EnableCache {
		s.mtx.Lock()
		s.cache[key] = record
		s.cacheHash[hash] = record
		s.mtx.Unlock()
	}
	return err
}
