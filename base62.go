// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

package urlshorten

import (
	"math/rand"
)

var b62Alphabet = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
var b62IndexTable = map[rune]int64{}

func init() {
	b62IndexTable = buildIndexTable(b62Alphabet)
}

// build alphabet index
func buildIndexTable(s []rune) map[rune]int64 {
	var table = make(map[rune]int64, len(s))
	for i := 0; i < len(s); i++ {
		table[s[i]] = int64(i)
	}
	return table
}

// DecodeBase62String convert an base-62 string back to interger
func DecodeBase62String(s string) int64 {
	var n int64
	for _, r := range s {
		n = (n * 62) + b62IndexTable[r]
	}
	return n
}

// EncodeToBase62String convert an interger to base-62 string
func EncodeToBase62(id int64) []rune {
	if id == 0 {
		return nil
	}
	var short = make([]rune, 0, 12)
	for id > 0 {
		var rem = id % 62
		id = id / 62
		short = append(short, b62Alphabet[rem])
	}
	// reverse
	for i, j := 0, len(short)-1; i < j; i, j = i+1, j-1 {
		short[i], short[j] = short[j], short[i]
	}
	return short
}

func EncodeToBase62String(id int64) string {
	var short = EncodeToBase62(id)
	rand.Shuffle(len(short), func(i, j int) {
		short[i], short[j] = short[j], short[i]
	})
	return string(short)
}
