# urlshorten
一个轻量级的短域名web服务实现(a lightweight URL shorten web service implementation)

# Dependency
* redis server
* Go toolchain (1.12+)

# Usage

* enable go module(GO111MODULE=on)
* clone repo https://github.com/ichenq/urlshorten
* `cd $GOPATH/github.com/ichenq/urlshorten`
* `go mod tidy && go install ichenq/urlshorten/cli`

# API Test

API接口
`https://open.prototyped.cn/api/shorten?url=https://mail.google.com`

JSON返回
```
{
    'code': 0,
    'url': 'https://open.prototyped.cn/go/gkldFteQb',
    'error': 'error message'
}
```