package main

import (
	"flag"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/ichenq/urlshorten"
	log "github.com/sirupsen/logrus"
	_ "net/http/pprof"
)

func main() {
	var now = time.Now()
	rand.Seed(now.UnixNano())

	var configPath string
	flag.StringVar(&configPath, "config", "config.hjson", "specify configuration file path")
	flag.Parse()

	var config = urlshorten.NewConfig()
	if err := config.LoadFromFile(configPath); err != nil {
		log.Panicf("Load config: %v", err)
	}
	if err := urlshorten.SetupLog(config.LogLevel, config.LogFilePath, true); err != nil {
		log.Panicf("Setup log: %v", err)
	}

	var storage = urlshorten.CreateStorageBy(config.Storage)
	if err := storage.Start(config); err != nil {
		log.Panicf("Init storage: %v", err)
	}

	if config.PprofPort > 0 {
		go profiler(uint16(config.PprofPort))
	}

	defer urlshorten.Catch()

	var server = urlshorten.NewHttpServer(storage, config)
	server.Start()
	log.Infof("server started at %s", config.Addr)
	var sigChan = make(chan os.Signal)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGABRT)
	for {
		select {
		case sig := <-sigChan:
			switch sig {
			case syscall.SIGINT, syscall.SIGABRT:
				log.Infof("signal %s received", sig)
				server.Shutdown()
				storage.Close()
				os.Exit(1)
			}
		}
	}
}

func profiler(port uint16) {
	addr := ":" + strconv.Itoa(int(port))
	log.Infof("pprof serving at http://%s/debug/pprof", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Errorf("http %v", err)
	}
}
