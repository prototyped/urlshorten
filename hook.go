// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

package urlshorten

import (
	"fmt"
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

type HookFileLog struct {
	filename string
}

func NewFileLogHook(filename string) logrus.Hook {
	return &HookFileLog{
		filename: filename,
	}
}

func (h *HookFileLog) Fire(entry *logrus.Entry) error {
	line, err := entry.String()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to read entry, %v", err)
		return err
	}

	var now = time.Now()
	var filename = fmt.Sprintf("%s_%d%02d%02d.log", h.filename, now.Year(), now.Month(), now.Day())
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	// since we know io.Write must not modify the data passed in
	var data = FastStringToBytes(line)
	_, err = f.Write(data)
	return err
}

func (h *HookFileLog) Levels() []logrus.Level {
	return logrus.AllLevels
}
