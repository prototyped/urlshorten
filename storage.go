// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

package urlshorten

type Storage interface {
	Start(*Config) error
	Close()

	// query shorten record by short url
	QueryUrlRecord(shortUrl string) (*UrlRecord, error)

	// query shorten record by long url
	QueryUrlRecordBy(longUrl string) (*UrlRecord, error)

	// save shorten record to DB
	SaveShortenUrl(record *UrlRecord) error
}

func CreateStorageBy(name string) Storage {
	switch name {
	case "redis":
		return NewRedisStore()
	case "mysql":
		return NewMySQLStorage()
	}
	return nil
}

type UrlRecord struct {
	ID         int64  `json:"id"`
	ShortUrl   string `json:"short_url"`   //短地址
	LongUrl    string `json:"long_url"`    //长地址
	Hash       string `json:"hash"`        //长地址hash
	Ttl        int    `json:"ttl"`         //到期时间
	ApiKey     string `json:"api-key"`     //API认证
	CreateIP   string `json:"create_ip"`   //创建IP
	CreateTime string `json:"create_time"` //创建时间
}
